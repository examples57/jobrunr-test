package alex.test;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

@Log4j2
@RequiredArgsConstructor
public abstract class Task {

    public void process(String id) {
        processImpl(id);
    }

    protected abstract void processImpl(String id);
}

@Component
@Log4j2
class Task1 extends Task {

    @Override
    protected void processImpl(String id) {
        log.info("process task1");
    }

}

@Component
@Log4j2
class Task2 extends Task {

    @Override
    protected void processImpl(String id) {
        log.info("process task2");
    }

}

